    //===========================================================================//
    //
    // jQuery hideOpacity / fadeInOpacity
    //
    //===========================================================================//
    /*!
        jQuery's hide() and fadeIn() functions do not work well with elements
        with CSS display : inline-block.  jQuery will reset the display type to
        "block".
        
        These are quick replacements that are solely based on the "opacity" 
        property (leaving "display" alone).
        
        Note: these are *not* complete replacements.  They make no effect to support
        the full calling conventions and options of fadeIn() / hide().  Rather
        than going that route, it'd be better to fix jQuery itself to support 
        "inline-block".  These are intended as the quick workaround if / until
        that gets done.
     */
    $["fn"]["hideOpacity"] = function(options) {
        return this.each(function() {
            $(this).css("opacity", 0);
        });
    };
    