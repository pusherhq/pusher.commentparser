/*!
  This is the brief comment. It should include the whole paragraph
  even if there are multiple lines.
  
  @status in development!

  The key above should not interrupt processing of the description.
  The description should include both the first paragraph, this one,
  and the next paragraph.
  
  This is another paragraph of text.
  
  @multiline Test that multiple line keys are
             working properly.
  
  @todo Todo 1
  @todo Todo 2
  @todo Todo 3
  
  @options.first    First
  @options.second   Second
  @options.third    Third
  
  @options.alpha.empty
  @options.alpha.notempty   Not Empty!
  @options.beta.empty
  @options.gamma.notempty Not Empty!
  @options.beta.notempty Not Empty!
  
  @options.gamma.empty        
*/
function test1()
{
}


/*!
  @brief This is the brief comment. 
  
  This is the full description (should not include the explicit brief).
*/
function test1()
{
}

