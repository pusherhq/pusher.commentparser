
//===========================================================================//
//
// Geometry Classes
//
//===========================================================================//

/*!
    @group  Geometry
    @name   Geometry
    @api    new Geometry()
    @type   class
    
    Base class for all geometry.  Not intended for direct use, but 
    rather for use in expressions such as ``obj instanceof Geometry``.
 */
var Geometry = function()
{
};

/*!
    @group  Geometry
    @name   Circle
    @api    new Circle (x, y, radius)
    @type   class    
    
    
 */
var Circle = function (x, y, radius)
{
    this._x = x;
    this._y = y;
    this._radius = radius;
};
(function (methods)
{
    methods.draw = function (ctx)
    {
        ctx.beginPath();
        ctx.arc(this._x, this._y, this._radius, 0, 2 * Math.PI, false);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    };
    
})(Circle.prototype = new Geometry);

/*!
    @group  Geometry
    @name   Arc
    @type   class    
 */
var Arc = function (x, y, innerRadius, outerRadius, startAngle, endAngle)
{
    this._center = [x , y];
    this._innerRadius = innerRadius;
    this._outerRadius = outerRadius;
    this._startAngle = startAngle;
    this._endAngle  = endAngle;
};

(function(methods)
{
    methods.draw = function (ctx)
    {
        var x    = this._center[0];
        var y    = this._center[1];
        var ri   = this._innerRadius;
        var ro   = this._outerRadius;            
        var ang0 = this._startAngle;
        var ang1 = this._endAngle;
        
        var cos0 = Math.cos(ang0);
        var sin0 = Math.sin(ang0);
        var cos1 = Math.cos(ang1);
        var sin1 = Math.sin(ang1);
                    
        
        ctx.beginPath();
        ctx.moveTo(x + ri * cos0, y + ri * sin0);
        ctx.arc(x, y, ri, ang0, ang1, false);
        ctx.lineTo(x + ro * cos1, y + ro * sin1);
        ctx.arc(x, y, ro, ang1, ang0, true);            
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    };

})(Arc.prototype = new Geometry);


//===========================================================================//
//
// Visualization
//
//===========================================================================//

//---------------------------------------------------------------------------//
// Instance
//---------------------------------------------------------------------------//

/*!
    @group  Visualization
    @name   Instance
 */
var Instance = function(options)
{
    this.style    = {};
    this.geometry = null;
    this._updates = [];
    this.data = {};
    
    _.extend(this, options);
};
(function (methods)
{
    methods.update = function ()
    {
        var self = this;
        _.each(self._updates, function(f) {
            f.call(self);
        });
    };
            
})(Instance.prototype);

//---------------------------------------------------------------------------//
// List
//---------------------------------------------------------------------------//

/*!
    @group  Visualization
    @name   List
 */
var List = function()
{
    this._list = [];
    this._style = {};
};
(function (methods)
{
    methods.forEach = function (iterator, context)
    {
        _.each(this._list, iterator, context);
    };
    
    methods.get = function (index)
    {
        return this._list[index];
    };
    
    methods.addOne = function (options)
    {
        if (arguments.length === 3)
        {
            options = 
            {
                geometry    : arguments[0],
                transform   : arguments[1],
                style       : arguments[2]
            };
        }    
        else if (arguments.length === 2)
        {
            options = 
            {
                geometry    : arguments[0],
                style       : arguments[1]
            };
        }
        else if (options instanceof Geometry)
        {
            options = { geometry : options };
        }       
    
        if (typeof options.style === "string")
            options.style = { fillStyle : options.style };
       
        var inst = new Instance(options);
        this._list.push(inst);
        return inst;
    };    
    
    methods.add = function (options)
    {       
        var self = this;
        
        if (arguments.length == 1 && _.isArray(options))
            return _.map(options, function(o) { return self.addOne(o); });
        else
            return self.addOne.apply(this, arguments);
    };
    
    methods.style = function (options)
    {
        _.extend(this._style, options);
    };

})(List.prototype);

//---------------------------------------------------------------------------//
// Visualization
//---------------------------------------------------------------------------//

/*!
    @group  Visualization
    @name   Visualization
 */
var Visualization = function(canvas, list)
{
    this._canvas = canvas;
    this._ctx = canvas.getContext("2d");
    this._list = list || new List;
};

(function (methods, statics)
{
    statics.viewScale = [ 1, 1 ];

    methods.list = function() {
        return this._list;
    };

    methods.update = function() {
        this._list.forEach(function (inst)
        {
            inst.update();
        });
    };
        
    methods.redraw = function() {
        
        var ctx = this._ctx;
        var width = this._canvas.width;
        var height = this._canvas.height;
        
        ctx.save();
            ctx.clearRect(0,0, width, height);
            
            ctx.scale(width/2, -height/2);
            ctx.translate(1, -1);            
            ctx.scale(-1/Visualization.viewScale[0], 1/Visualization.viewScale[1]);
            
            ctx.lineWidth = 0.001;
        
            ctx.save();
            _.each(this._list._style, function (value, key) {
                ctx[key] = value;
            });
            this._list.forEach(function (inst)
            {
                ctx.save();
                _.each(inst.style, function (value, key) {
                    ctx[key] = value;
                });
                inst.geometry.draw(ctx);
                ctx.restore();
            });
            ctx.restore();
            
        ctx.restore();
    };
    
    methods.run = function()
    {
        var self = this;        
        self._timer = window.setInterval(function() 
        {
            try
            {
                self.update();
                self.redraw();            
            } 
            catch (e)
            {
                window.clearInterval(self._timer);
                throw e;
            }
        }, 20);
    };
    
})(Visualization.prototype, Visualization);


//===========================================================================//
//
// Plug-ins
//
//===========================================================================//

/*!
    @group  Plug-ins
    @name   animateCycleHue
 */
Instance.prototype.animateCycleHue = function (options)
{
    if (typeof options == "number")
        options = { amount : options };

    var opt = _.extend({
        amount : 0.2
    }, options);
    

    this.data.hsv = pusher.color(this.style.fillStyle).hsv();
    
    var update = function ()
    {
        var hsv = this.data.hsv;
        hsv[0] = (hsv[0] + opt.amount) % 360;
        this.style.fillStyle = pusher.color("hsv", hsv).html();
    };
    this._updates.push(update);
    
    return this;
}

/*!
    @group  Plug-ins
    @name   rotateArc
    
    Animates the arc so that is rotates about its center-point.
    
    @option.speed  The speed of the rotation
    @option.ccw    If true, rotates counter-clockwise; else clockwise.
    
    @todo   Add option to control the rotation center point.
 */
Instance.prototype.rotateArc = function (options)
{
    if (!this.geometry instanceof Arc)
        return this;
        
    if (typeof options == "number")
        options = { speed : options };
    else if (typeof options === "boolean")
        options = { ccw : options };

    var opt = _.extend({
        speed   : 0.005,
        ccw     : true
    }, options);
    
    
    var step = (opt.ccw ? 1 : -1) * opt.speed;
    var update = function ()
    {
        this.geometry._startAngle += step;
        this.geometry._endAngle += step;
    };
    this._updates.push(update);
    
    return this;
}
