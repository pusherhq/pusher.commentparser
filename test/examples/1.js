    //-----------------------------------------------------------------------//
    // Perlin Noise
    //-----------------------------------------------------------------------//
    /*!
        @name   perlinNoise
        @api    perlinNoise (x, y, z) -> Number        
        @status stable
       
        Returns a [0,1) value of a 3d Perlin noise function
        
        The Perlin Noise algorithm is copyright Ken Perlin.

        This implementation is based on, but not fully equivalent to, Malcolm 
        Kesson's freely available reference implementation provided at:
        http://www.fundza.com/c4serious/noise/perlin/perlin.html
     */
    lib.perlinNoise = (function() {

        /*
            Simple linear interpolation helper function.
         */
        function interpolate (t, a, b) { 
            return a + t * (b - a); 
        }

    }