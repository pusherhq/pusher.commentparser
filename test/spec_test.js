var parser = require("./../lib/main.js");
var should = require('chai').should();
var expect = require('chai').expect;
var fs     = require('fs');
var _      = require("underscore");

describe("Argument types", function(){
    
    it ("No arguments", function() {
        expect(function() { parser()}).to.throw(Error);
    });
    it ("Null argument", function() {
        expect(function() { parser(null)}).to.throw(Error);
    });
    it ("Number argument", function() {
        expect(function() { parser(0)}).to.throw(Error);
    });
    it ("Array argument", function() {
        expect(function() { parser([])}).to.throw(Error);
    });
    it ("Object argument", function() {
        expect(function() { parser({})}).to.throw(Error);
    });
    it ("Empty string", function() {
        expect(parser("")).to.deep.equal({ blocks : [] });
    });
    it ("Whitespace string", function() {
        expect(parser("     ")).to.deep.equal({ blocks : [] });
    });
});

describe("Javascript Comment Block Parsing and Extraction", function(){   

    it("should return an object", function(){
        expect(parser("/*! comment block */")).should.be.a('object');
    });

    it("should return line number", function(){
        expect(parser("/*! comment block */").blocks[0].line).equal(1);
    });

    it("should return json object", function(){
        expect(parser("/*! comment block */").blocks[0].json).should.be.a('object');
    });

    it("should parse two comment blocks but return the first", function(){
        var result = parser("/*! comment block 1 */\n/*! comment block 2 */");
        expect(result.blocks[0].json).should.be.a('object');
    });

    it("should return the comment above a function", function(){
        var result = parser("/*! First para \n @todo Hello */\n function install(){\n}");
        expect(result.blocks[0].json.brief).equal("First para");
    });

    it("should return the last comment, after the function", function(){

        var code = "/*! comment above */\n \
            function make(){\n \
            }\n \
            /*! comment after */";

        expect(parser(code).blocks[1].json.brief).equal("comment after");
    });

    it("should return a brief and description key", function(){
        var code = "/*! some random brief \n\n\n another world */";
        expect(parser(code).blocks[0].json.brief).equal("some random brief");
        expect(parser(code).blocks[0].json.description).equal([
            "some random brief",
            "another world"
        ].join("\n\n"));
    });

    it("should return a name-spaced key", function(){
        var code = "/*! @key.first Wonder World */";
        expect(parser(code).blocks[0].json.key.first).equal("Wonder World");
    });

    it("should return the proper line number", function(){
        var code = "/*! @key Wonder World */";
        expect(parser(code).blocks[0].line).equal(1);

        var code = "\n/*! @key.first Wonder World */";
        expect(parser(code).blocks[0].line).equal(2);

        var code = "\n\n\n/*! @key.first Wonder World */";
        expect(parser(code).blocks[0].line).equal(4);
    });
    
    it("Unterminated block", function(){
        expect(function() { parser("/*! ") }).to.throw(Error);
    });
});


describe("Parse example file (1.js)", function(){

    beforeEach(function(){
        var contents = fs.readFileSync(__dirname + '/examples/1.js', 'utf-8');
        contents = String(contents);
        this.result = parser(contents).blocks[0];
    });

    it("should return name attribute.", function(){
        expect(this.result.json.name).equal("perlinNoise");
    });

    it("should return brief attribute.", function(){
        expect(this.result.json.brief).equal("Returns a [0,1) value of a 3d Perlin noise function");
    });

    it("should return description attribute.", function(){
        expect(this.result.json.description).equal([
            "Returns a [0,1) value of a 3d Perlin noise function",
            "The Perlin Noise algorithm is copyright Ken Perlin.",
            "This implementation is based on, but not fully equivalent to, Malcolm Kesson's freely available reference implementation provided at: http://www.fundza.com/c4serious/noise/perlin/perlin.html"
        ].join("\n\n"));
    });

    it("should return status attribute.", function(){
        expect(this.result.json.status).equal("stable");
    });

    it("should return api attribute.", function(){
        expect(this.result.json.api).equal("perlinNoise (x, y, z) -> Number");
    });

    it("should return correct line number.", function(){
        expect(this.result.line).equal(4);
    });

    it("full description includes brief.", function(){
        var firstLineOfFull = this.result.json.description.split(/\n/)[0];
        expect(firstLineOfFull).equal("Returns a [0,1) value of a 3d Perlin noise function");
    });
    
});


describe("Parse example file (2.js)", function(){

    beforeEach(function(){
        var contents = fs.readFileSync(__dirname + '/examples/2.js', 'utf-8');
        contents = String(contents);
        var results = parser(contents);
        this.r0 = results.blocks[0].json;
        this.r1 = results.blocks[1].json;
    });
    
    if("status key is correct.", function(){
        expect(this.r0.status).equal("in development!");
    });
    
    it("brief comment is correct.", function(){
        expect(this.r0.brief).equal("This is the brief comment. It should include the whole paragraph even if there are multiple lines.");
    });

    it("full description has correct whitespace.", function(){
        expect(this.r0.description).equal([
            "This is the brief comment. It should include the whole paragraph even if there are multiple lines.",
            "The key above should not interrupt processing of the description. The description should include both the first paragraph, this one, and the next paragraph.",
            "This is another paragraph of text."
        ].join("\n\n"));
    });
    
    it("multiline keys are joined correctly.", function(){
        expect(this.r0.multiline, "Test that multiple line keys are working properly.");
    });
   
    it("array keys are working.", function(){
        expect(this.r0.todo[0]).equal("Todo 1");
        expect(this.r0.todo[1]).equal("Todo 2");
        expect(this.r0.todo[2]).equal("Todo 3");
    });
    
    it("nested keys are working.", function(){
        expect(this.r0.options.first).equal("First");
        expect(this.r0.options.second).equal("Second");
        expect(this.r0.options.third).equal("Third");
    });
    
    it("doubly nested key alpha.", function(){
        expect(this.r0.options.alpha.empty).equal("");
        expect(this.r0.options.alpha.notempty).equal("Not Empty!");
    });
    
    it("doubly nested keys are working.", function(){
        expect(this.r0.options.beta.empty).equal("");
        expect(this.r0.options.beta.notempty).equal("Not Empty!");
        expect(this.r0.options.gamma.empty).equal("");
        expect(this.r0.options.gamma.notempty).equal("Not Empty!");
    });
    
    it("second comment block brief is correct", function(){
        expect(this.r1.brief).equal("This is the brief comment.");
    });
    
    it("second comment block description is correct", function(){
        expect(this.r1.description).equal("This is the full description (should not include the explicit brief).");
    });
});

describe("Parse example file (3.js)", function(){

    beforeEach(function(){
        var contents = fs.readFileSync(__dirname + '/examples/3.js', 'utf-8');
        contents = String(contents);
        
        this.expected = { "brief" : "This is the brief value.", "description" : "ALL of the text that is not associated with a key is the description value.", "status" : "alpha" };
        this.result = parser(contents).blocks[0].json;
    });

    it("brief comment is correct.", function(){
        expect(this.result.brief).equal(this.expected.brief);
    });
    it("description is correct.", function(){
        expect(this.result.description).equal(this.expected.description);
    });
    it("status is correct.", function(){
        expect(this.result.status).equal(this.expected.status);
    });
    
});

describe("Parse example file (4.js)", function(){

    beforeEach(function(){
        var contents = fs.readFileSync(__dirname + '/examples/4.js', 'utf-8');       
        this.result = parser(contents);
        
        var name = {};
        _.each(this.result.blocks, function (block) {
            name[block.json.name] = block.json;
        });
        this.name = name;
    });
    
    it("Geometry type", function() { expect(this.name.Geometry.type).equal("class"); });
    it("Geometry group", function() { expect(this.name.Geometry.group).equal("Geometry"); });
    it("Geometry brief", function() { 
        expect(this.name.Geometry.brief).equal(
            "Base class for all geometry. Not intended for direct use, but rather for use in expressions such as ``obj instanceof Geometry``."
        ); 
    });

    it("Circle type", function() { expect(this.name.Circle.type).equal("class"); });
    it("Circle group", function() { expect(this.name.Circle.group).equal("Geometry"); });
    it("Circle brief", function() { 
        expect(this.name.Circle.brief).to.be.undefined;
    });
    
    it("rotateArc option.speed", function() { expect(this.name.rotateArc.option.speed).equal("The speed of the rotation"); });
    it("rotateArc option.ccw", function() { expect(this.name.rotateArc.option.ccw).equal("If true, rotates counter-clockwise; else clockwise."); });
    
});

describe("Parse example file (5.js)", function(){

    it("file parses as expected.", function(){
        var source = fs.readFileSync(__dirname + '/examples/5.js', 'utf-8');
        var json   = fs.readFileSync(__dirname + '/examples/5.json', 'utf-8');
        
        var result = parser(source);
        var expected = JSON.parse(json);
        expect(result).to.deep.equal(expected);
    });
    
});

