#! /usr/bin/env node

var parser = require("../lib/main.js");
var fs     = require("fs");

var content = fs.readFileSync(process.argv[2]).toString();
var result = parser(content);
console.log( JSON.stringify(result, null, 2) );
