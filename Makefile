CMD = node_modules/mocha/bin/mocha

test: 
	$(CMD) test/spec_test -R spec --ignore-leaks

.PHONY: test