(function (global, console, undefined) {
    
    /**
     * Trim leading and trailing whitespace
     */
    var trimWhitespace = function(s)
    {
        return s.replace(/^\s+/, "").replace(/\s+$/, "");
    }
    
    /**
     * Check if the key exists already:
     * If not, set it; otherwise, ensure it's an array and
     * append the new value.
     */    
    var setOrAppendValue = function (object, key, value)
    {
        var current = object[key];
        var typeofKey = typeof current;
        
        if (typeofKey == "undefined")
            object[key] = value;
        else if (typeofKey == "object")
            object[key].push(value); 
        else 
            object[key] = [ current, value ];
         
        return object;
    }
    
    var setOrAppendNestedValue = function (object, key, value)
    {
        //
        // Walk the complex key, a piece at a time and ensure
        // the base object has all those properties until we
        // reach the final key -- at which point it's a simple
        // setOrAppendValue.
        //
        var base = object;
        var parts = key.split(".");        
        while (parts.length > 1)
        {
            var p = parts.shift();
            
            if (typeof base[p] === "undefined")
                base[p] = {};                
            base = base[p];
        }
        
        setOrAppendValue(base, parts[0], value);
        return object;
    }
    
    /**
     * Remove the beginning line breaks in a string.
     * @param  {String} string Passed string.
     * @return {String}        Parsed string.
     */
    function removeBegLineBreaks(string) {
        string = string.replace(/^ ?[\n]/, "");
        var m = string.match(/^ ?[\n]/);
        if (m) {
            return removeBegLineBreaks(string);
        }
        return string.replace(/^ /, "");
    }
    
    /*
        The processing has two parts:
        - Convert single newlines into spaces
        - Remove empty paragraphs from the head and tail of the array
        
        Return the modified array.
     */
    var processParagraphArray = function(paragraphs)
    {           
        // Convert single newlines to spaces
        for (var i = 0; i < paragraphs.length; ++i)
            paragraphs[i] = trimWhitespace(paragraphs[i].replace(/\n{1}/g, " "));
    
        //
        // Remove any empty paragraphs preceding real content
        //
        var isEmptyParagraph = function (p) {
            return (p === "" || removeBegLineBreaks(p) === "");
        };                  
        while (paragraphs.length && isEmptyParagraph(paragraphs[0]))
            paragraphs.shift();            
        while (paragraphs.length && isEmptyParagraph(paragraphs[paragraphs.length - 1]))
            paragraphs.pop();
    
        return paragraphs;
    }
    
    /*
        Extract an array of comment blocks and the associated line number.
     */
    var extractCommentBlocks = function (code)
    {
        // Collect the resulting blocks here
        var blocks = [];
        
        var pos = 0;
        do
        {
            //
            // Find the start and end of each comment block, then extract the
            // substring of the text.
            //
            var start = code.indexOf("/*!", pos);
            var end = -1;
            if (start != -1)
            {
                start += 3;
                end = code.indexOf("*/", start);
                if (end != -1)
                {
                    // Not a super-efficient way of computing the line number, but
                    // works correctly: count the newlines before the current position
                    var lineNumber = code.substr(start).split("\n") + 1;
                    
                    blocks.push({ 
                        line    : lineNumber, 
                        content : code.substr(start, end - start) 
                    });
                    pos = end + 2; 
                }
            }
            
            if (end == -1)
                pos = code.length;
           
        } while (pos < code.length);
        
        return blocks;
    }

    /**
     * Parsing function that takes a string parameter and returns
     * an object containing the parsed comment blocks (special notation though).
     *
     * @param  {String} code Contains the code to parse.
     * @return {Object}      The parsed comment blocks and attributes.
     */
    module.exports = function (code) {
        // Define a few variables up-top.
        var value, key, nonAttr, para;

        /**
         * Replaces extra whitespace
         * @param  {String} val String we want to replace
         * @return {String}     Outputted string.
         */
        function replace(val, nonAttributes) {
            val = val.split(/\n\n/)[0];
            /**var m = val.match(/\n/);
            if (m) {
                var split = val.replace(/ /g, "").split(/\n\n/);
                val = split[0];
            }**/
            val = val.replace(/([.\(\)\\])/gm, "\\$1");
            var reg = new RegExp("^@(.+) ( ?" + val + " ?)", "i", "m");
            nonAttributes = nonAttributes.replace(reg, "");
            return nonAttributes;
        }

        /**
         * Recursively loop through an object until we hit
         * a string, then we can call `replace` on that string.
         *
         * @param  {Object} val`
         * @return {null}
         */
        function loop(val) {
            for (var q in val) {
                if (val.hasOwnProperty(q)) {
                    var qvalue = val[q];
                    if ("string" === typeof qvalue) {
                        nonAttr = replace(qvalue, nonAttr);
                    } else {
                        loop(qvalue);
                    }
                }
            }
        }

        /**
         * If code isn't defined as an argument of the "Parser"
         * function, then throw an Error
         */
        if ("undefined" === typeof code || null === code) {
            throw Error("Missing code to parse.");
        }
        
        /**
         * We need to replace all urls with a temporary '__DEL__' delimeter
         * that we can use later to undo this. (The regexs used below
         * don't work with urls.)
         *
         */
        code = code.replace(new RegExp("(http|ftp|https)://[\\w-]+(\\.[\\w-]+)+([\\w-.,@?^=%&:/~+#-]*[\\w@?^=%&;/~+#-])?", "gim"), function () {
            return arguments[0].replace(new RegExp("/", "g"), "__DEL__");
        });
        /**
         * The regular expression to parse and match special block comments.
         * @type {RegExp}
         */
        var blockCommentRegex = new RegExp("\\/\\*\\! ?([^\\*\\/]*) ?\\*\\/", "gim");

        /**
         * Define the attribute regular expression for later use.
         * @type {RegExp}
         */
        var attrRegex = new RegExp("@([^@ ][^@]+)", "gmi");
        /**
         * Define another kind of attribute regular expression.
         * @type {RegExp}
         */
        var singleAttrRegex = new RegExp("^@([^@ ][^@]+)");
        /**
         * Define the "global" blocks array that will hold each comment
         * block independently.
         *
         * @type {Array}
         */
        var blocks = [];
        
        /**
         * Only continue if we matched a comment block.
         */
        var rawBlocks = extractCommentBlocks(code);

        if (rawBlocks.length > 0) {
        
            var match = [];
            rawBlocks.forEach(function (block) {
                match.push(block.content);
            });
        
            /**
             * Loop through each comment block occurrence.
             */
            match.forEach(function (block, indexNumber) {
                var attributes = {}, blockObject, attrMatch;
                /**
                 * Return the current loop if the block is empty.
                 * @type {null}
                 */
                if (null === block || " " === block) {
                    return;
                }
                /**
                 * Replace the current block with the previous regex. The match
                 * call doesn't give us the correct output.
                 *
                 * @type {String}
                 */
                block = block.replace(blockCommentRegex, "$1");
                /**
                 * Start the blockObject object that will be filled out later.
                 * @type {Object}
                 */
                blockObject = {
                    line: 0, // We insert the line number later.
                    json: {} // Same thing with the json.
                };
                /**
                 * Match the attributes within the current comment block
                 * @type {Array}
                 */
                attrMatch = block.match(attrRegex);
                /**
                 * Start the `nonAttr` string and set it to the old block
                 * string.
                 * @type {[type]}
                 */
                nonAttr = block;
                /**
                 * Continue only if we matched an attribute.
                 */
                if (attrMatch) {
                    /**
                     * Loop through each found occurrence of the attributes.
                     */
                    attrMatch.forEach(function (attr) {
                        var singleAttrMatch, words, values, dotMatch, namespaces;
                        
                        attr = trimWhitespace(attr);
                        
                        /**
                         * Match a single occurrence of an attribute.
                         * @type {Array}
                         */
                        singleAttrMatch = attr.match(singleAttrRegex);
                        
                        if (singleAttrMatch) 
                        {                            
                            //
                            // Get the unprocessed match and remove it from the "not associated with
                            // an attribute" part of the text, since we *are* processing it as an
                            // attribute.
                            //
                            var baseMatch = singleAttrMatch[1];
                            var matchPara = baseMatch.split(/\n\n/)[0];
                            var fullPara = singleAttrMatch[0].split(/\n\s*\n/)[0];
                            nonAttr = nonAttr.replace(fullPara, "");
                            
                            /**
                             * Get the first index of the singleAttrMatch array
                             * and split the string by a whitespace delimiter.
                             * @type {Array}
                             */
                            var content  = matchPara.replace(/[ \t]+/g, ' ').replace(/(\n) ?(\n) ?/gmi, "$1$2");
                            var words = content.split(/ /);
                            var key = words.shift();

                            /**
                             * Re-join the words by a whitespace delimiter to
                             * create a proper structure.
                             * @type {String}
                             */
                            var values = trimWhitespace( words.join(" ").split(/\n\n/)[0] );
                            setOrAppendNestedValue(attributes, key, values);
                            
                        } // End of singleAttrMatch match.
                    }); // End of attribute loop.
                } // End of attribute matching.
                
                var attributeKey;
                /**
                 * Loop through each attribute
                 */
                for (attributeKey in attributes) {
                    /**
                     * Verify that the object owns the props.
                     */
                    if (attributes.hasOwnProperty(attributeKey)) {
                        /**
                         * Set the value
                         * @type {String/Object/Array}
                         */
                        value = attributes[attributeKey];
                        /**
                         * Check if the type of the value is a string or not.
                         */
                        if ("string" === typeof value) {
                            // We need to use the replace function on the value.
                            nonAttr = replace(value, nonAttr);
                        } else {
                            // Do a recursion loop until we find the string value.
                            loop(value);
                        }
                    }

                }

                var att = nonAttr.replace(/[ \t]+/g, ' ').replace(/(\n) ?(\n) ?/gmi, "$1$2").split(/\n\n/g);
                var finalArray = [];
                for (i in att) {
                    var array = att[i].split(/\n/g);
                    var f = att[i];

                    if (f.match(/^@(.+) ([\s\S]+)/gim)) {
                        f = f.replace(/^@(.+) ([\s\S]+)/gim, "");
                    }

                    array.forEach(function (key) {
                        var key = removeBegLineBreaks(key.replace(/[ \t]+/g, ' ').replace(/^ /, ""));
                        var m = key.match(/^@(.+) ([\s\S]+)/gim);
                        if (m) {
                            f = f.replace(m[0], "");
                        }
                    });

                    finalArray.push(f);
                }
                nonAttr = finalArray.join("\n\n");
                nonAttr = nonAttr.replace(/(\n) ? ?(\n)/g, "$1$2");
                
                /**
                 * Check if the brief attribute has been set or not.
                 */
                if ("undefined" === typeof attributes.brief) {
                    /**
                     * Get the nonAttr variable set previously
                     * and split by paragraph (double line break)
                     * @type {Array}
                     */
                    var originalParaTemp = nonAttr.split(/\n\n/);
                    var originalPara = [];
                    originalParaTemp.forEach(function (key, value) {
                        if (key) {
                            originalPara.push(key);
                        }
                    });                          
                    originalPara = processParagraphArray(originalPara);                    
                    
                    if ("undefined" !== typeof originalPara[0]) {

                        para = originalPara[0].split(/\n/);                       
                        
                        var result = [];
                        
                        /**
                         * Loop through each `para` indexes.
                         */
                        para.forEach(function (el) {
                            /**
                             * Make sure it's a valid value.
                             * @type {}
                             */
                            if (null === el || "" === el || "" === el.replace(/\s{2,}/g, '')) {
                                return;
                            }
                            /**
                             * Push the current value to the result array as it's
                             * a valid value.
                             */
                            result.push(el);
                        });
                        /**
                         * Set the brief attribute.
                         * We need to remove any whitespace at the beginning or the
                         * end of the string as well as repeating whitespace.
                         *
                         * @type {String}
                         */                         
                        attributes.brief = result.join("\\n")
                            .replace(/ $/, "") // Whitespace at the end of the string.
                            .replace(/\s{2,}/g, ' ') // Repeating whitespace.
                            .replace(/^ /, "") // Whitespace at the beginning of the string.
                            ;
                        /**
                         * Check the validity of the description attribute.
                         */
                        if ("undefined" === typeof attributes.description) {
                                                
                            /**
                             * Get the description and split by double line breaks
                             * and replace end whitespace.
                             * @type {String}
                             */
                            var val = trimWhitespace(originalPara.join("\n\n"));
                            
                            /**
                             * Set the description attribute and so some major string
                             * replacement and modifications.
                             * @type {[type]}
                             */
                            attributes.description = val.replace(/[ \t]+/g, ' ') // Get rid of extra whitespace.
                            .replace(/^ /, "") // Whitespace at the start of the string.
                            .replace(/ $/, "") // Whitespace at the end of the string.
                            .replace(/([\n])(?=[\S\s])/mg, "$1") // line break preceding by whitespace.
                            .replace(/(\n) (\n)/g, "$1$2") // Two line breaks split by a single whitespace.
                            .replace(/\\n\\n/, "")
                            .replace(/\\n/g, "\n");
                            ;
                            /**
                             * Remove beginning line breaks for the descriptions, as
                             * it sometimes picks up a ton of them.
                             * @type {String}
                             */
                            attributes.description = removeBegLineBreaks(attributes.description);
                        } // End of description attribute.

                    }
                } else { // End of brief attribute.
                
                    if ("undefined" === typeof attributes.description) {
                        /**
                         * Get the nonAttr variable set previously
                         * and split by paragraph (double line break)
                         * @type {Array}
                         */
                        var originalPara = nonAttr.split(/\n\n/);      
                        originalPara = processParagraphArray(originalPara);
                        
                        if (originalPara.length)
                        {
                            /**
                             * Split the first index instead.
                             * @type {Array}
                             */
                            para = originalPara[0].split(/\n/);
                            
                            /**
                             * Get the description and split by double line breaks
                             * and replace end whitespace.
                             * @type {String}
                             */
                            var val = originalPara.join("\n\n").replace(/ $/, "");

                            /**
                             * Set the description attribute and so some major string
                             * replacement and modifications.
                             * @type {[type]}
                             */
                            attributes.description = val.replace(/[ \t]+/g, ' ') // Get rid of extra whitespace.
                            .replace(/^ /, "") // Whitespace at the start of the string.
                            .replace(/ $/, "") // Whitespace at the end of the string.
                            .replace(/([\n])(?=[\S\s])/mg, "$1") // line break preceding by whitespace.
                            .replace(/(\n) (\n)/g, "$1$2") // Two line breaks split by a single whitespace.
                            .replace(/\\n\\n/, "")
                                .replace(/\\n/g, "\n");
                            /**
                             * Remove beginning line breaks for the descriptions, as
                             * it sometimes picks up a ton of them.
                             * @type {String}
                             */
                            attributes.description = removeBegLineBreaks(attributes.description);
                        }
                    } // End of description attribute.

                }

                var attribureKeyLoop, attrValue;
                /**
                 * Loop through each attribute once again.
                 */
                for (attribureKeyLoop in attributes) {
                    if (attributes.hasOwnProperty(attribureKeyLoop)) {
                        attrValue = attributes[attribureKeyLoop];
                        if ("string" === typeof attributes[attribureKeyLoop]) {
                        
                            // Replace the previous url delimiter with it's appropriate characters.
                            attrValue = attrValue
                                .replace(/__DEL__/g, "/")
                                .replace(/\n$/gmi, "")
                                .replace(/^\n/gmi, "")
                                .replace(/^ /gmi, "")
                                .replace(/ $/gmi, "");
                            
                            // Due to some confusion in the feature spec, the newline characters are 
                            // overly escaped. Patch things up a bit here until the full change is
                            // done:
                            attrValue = attrValue
                                .replace(/\n/g, "\n\n")
                                .replace(/^\s+/,"")
                                .replace(/\s+$/,"")
                                ;
                            
                            attributes[attribureKeyLoop] = attrValue;
                        }
                    }
                }

                /**
                 * Push the new finished block to the global block array.
                 */
                blockObject.json = attributes;
                blocks.push(blockObject);
                
            }); // End of block loop.
        } // End of block match.
        // Set the blockNumber to 0.
        var blockNumber = 0;
        /**
         * Split the whole code by line and loop through each line.
         */
        code.split(/\n/).forEach(function (line, number) {
            /**
             * Match a special opening comment block.
             */
            if (line.match(/\/\*\!/)) {
                /**
                 * Increment the blockNumber by one.
                 * @type {Number}
                 */
                blockNumber += 1;
                
                /**
                 * Set the line number.  If the blockNumber exceeds
                 * the array size, throw an error that this is an 
                 * unhandled case.  It's likely an unterminated comment
                 * or another "unlikely" case.
                 * @type {Number}
                 */
                 if (blockNumber > blocks.length)
                    throw new Error("Could not parse comment block around line " + (number + 1));
                 else
                    blocks[blockNumber - 1].line = number + 1;
            }
        }); // End of line loop.
        /**
         * Return the main object.
         * @type {Object}
         */
        return {
            blocks: blocks
        };
    }; // End of function.
})(global, console);